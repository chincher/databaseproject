<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Installments;

class Loan extends Model
{
	protected $primaryKey = 'loan_id';
	protected $table = 'loan';
	protected $appends = ['paid_installments_count'];

	public function user()
	{
		return $this->belongsTo('App\User', 'id', 'user_id');
	}

	public function installments()
	{
		return $this->hasMany('App\Installments', 'loan_id', 'loan_id');
	}

	public function getPaidInstallmentsCountAttribute()
	{
		$installmentsCount = Installments::where([
			['loan_id', $this->loan_id],
			['status', 'paid']
		])->count();

		return $installmentsCount;
	}
}
