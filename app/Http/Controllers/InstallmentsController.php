<?php
namespace App\Http\Controllers;

use App\Deposit;
use App\Installments;
use App\Loan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;


class InstallmentsController extends Controller
{
	public function getUserInstallmentsList($loan_id)
	{
		$installments = Installments::where([
			['loan_id', $loan_id]
		])->get();

		return view('user.installments', ['installments' => $installments]);
	}

	public function payInstallment($installments_id)
	{
		$user = Auth::user();
		if (Installments::where([
			['installments_id', $installments_id],
			['user_id', $user->id],
			['status', 'not_paid']
		])->exists()
		) {
			Installments::where([
				['installments_id', $installments_id],
				['user_id', $user->id],
				['status', 'not_paid']
			])->update(['status' => 'paid']);

			$loan = Loan::join('installments', 'loan.loan_id', '=', 'installments.loan_id')
				->select('loan.*')
				->where([
					['installments.installments_id', $installments_id]
				])->first();

			if (($loan->installments_number == $loan->paid_installments_count) && ($loan->status == 'not_paid_out')) {
				Loan::where([
					['loan_id', $loan->loan_id]
				])->update(['status' => 'paid_out']);

				Deposit::where([
					['loan_id', $loan->loan_id]
				])->delete();

				$message = [
					['status' => 'success', 'message' => "Spłaciłeś ostatnią ratę. Twoja pożyczka została wypłacona. Otrzymujesz zwrot kwoty z depozytu."]
				];

				session()->flash('flash', $message);

				return back();
			} else if (($loan->installments_number == $loan->paid_installments_count) && ($loan->status == 'paid_out')) {
				Deposit::where([
					['loan_id', $loan->loan_id]
				])->delete();

				$message = [
					['status' => 'success', 'message' => "Spłaciłeś ostatnią ratę. Otrzymujesz zwrot kwoty z depozytu."]
				];

				session()->flash('flash', $message);

				return back();
			} else {

				$message = [
					['status' => 'success', 'message' => "Rata pożyczki została zapłacona."]
				];

				session()->flash('flash', $message);

				return back();
			}
		} else {
			$message = [
				['status' => 'error', 'message' => "Rata pożyczki jest zapłacona lub nie istnieje."]
			];

			session()->flash('flash', $message);

			return back();
		}
	}
}