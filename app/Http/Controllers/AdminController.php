<?php

namespace App\Http\Controllers;

use App\Loan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
	public static function getAllMoney()
	{
		$sumOfDeposit = DB::table('deposit')->sum('amount');

		$sumOfPaidInstallments = DB::table('installments')->where([
			['status', 'paid']
		])->sum('amount');

		$sumInterestsOfLostInstallments = DB::table('installments')->where([
			['status', 'lost']
		])->sum('interests');

		$sumOfPaidOutLoans = DB::table('loan')->where([
			['status', 'paid_out']
		])->sum('amount');

		$sum = ($sumOfDeposit + $sumOfPaidInstallments + $sumInterestsOfLostInstallments - $sumOfPaidOutLoans);

		return ['sum' => $sum, 'sumOfDeposit' => $sumOfDeposit];
	}

	public function adminPanel()
	{
		$money = self::getAllMoney();

		return view('admin.admin', ['sum' => $money['sum'], 'sumOfDeposit' => $money['sumOfDeposit']]);
	}

	public function getLoansList()
	{
		$money = self::getAllMoney();
		$mytime = date('Y-m-d H:i:s');

//		$loans = Loan::where([
//			['loan_end_date', '>', $mytime]
//		])->get();

		$loans = Loan::join('users', 'loan.user_id', '=', 'users.id')
			->select('loan.*', 'users.name', 'users.surname')
			->where([
				['loan_end_date', '>', $mytime]
			])
			->get();

		return view('admin.loans', ['loans' => $loans, 'sum' => $money['sum']]);
	}

	public function loanPaidOut($loan_id)
	{
		$money = self::getAllMoney();

		$loan = Loan::where([
			['loan_id', $loan_id]
		])->first();

		if ($loan->amount > $money['sum']) {
			$message = [
				['status' => 'danger', 'message' => "Pożyczka jest większa niż ilość dostępnych środków."]
			];

			session()->flash('flash', $message);

			return back();
		} else {
			Loan::where([
				['loan_id', $loan_id]
			])->update(['status' => 'paid_out']);

			$message = [
				['status' => 'success', 'message' => "Pożyczka została wypłacona."]
			];

			session()->flash('flash', $message);

			return back();
		}
	}

}