<?php

namespace App\Http\Controllers;

use App\Deposit;
use App\Installments;
use App\Loan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

define('INTEREST', 0.1);

class LoanController extends Controller
{
	public function getLoan()
	{
		return view('user.getLoan');
	}

	public function submitLoan(Request $request)
	{
		$user = Auth::user();

		$validator = Validator::make($request->all(), [
			'value' => 'required',
			'installments_count' => 'required',
		]);

		if ($validator->fails()) {
			return back()->withInput();
		} else {
			$loan = new Loan();
			$loan->amount = $request->input('value');
			$loan->interests = $request->input('value') * INTEREST;
			$loan->installments_number = $request->input('installments_count');
			$loan->user_id = $user->id;
			$loan->loan_end_date = date('Y-m-d', strtotime('+' . $request->input('installments_count') . ' month', strtotime(date("Y-m-d"))));
			$loan->save();

			$loan = Loan::where([
				['user_id', $user->id]
			])->orderBy('created_at', 'desc')->first();

			if ($loan->interests < 1000) {
				$depositAmount = 1000;
			} else {
				$depositAmount = $loan->interests;
			}

			$deposit = new Deposit();
			$deposit->amount = $depositAmount;
			$deposit->user_id = $user->id;
			$deposit->loan_id = $loan->loan_id;
			$deposit->save();

			$deposit = Deposit::where([
				['user_id', $user->id],
				['loan_id', $loan->loan_id]
			])->first();

			$singleLoanAmount = ($loan->amount + $loan->interests) / $loan->installments_number;
			$singleLoanInterests = $singleLoanAmount - ($loan->amount / $loan->installments_number);

			for ($i = 1; $i <= $loan->installments_number; $i++) {
				$installment = new Installments();
				$installment->user_id = $user->id;
				$installment->loan_id = $loan->loan_id;
				$installment->amount = $singleLoanAmount;
				$installment->interests = $singleLoanInterests;
				$installment->pay_time = date('Y-m-d', strtotime('+' . $i . ' month', strtotime(date("Y-m-d"))));
				$installment->save();
			}

			$installment = Installments::where([
				['loan_id', $loan->loan_id]
			])->first();

			return view('user.summary', ['loan' => $loan, 'deposit' => $deposit, 'installment' => $installment]);
		}
	}

	public function getUserLoansList()
	{
		$user = Auth::user();

		$loans = Loan::where([
			['user_id', $user->id]
		])->get();

		return view('user.loans', ['loans' => $loans]);
	}

	public function deleteLoan($loan_id)
	{
		$user = Auth::user();

		if (Loan::where([
			['loan_id', $loan_id],
			['user_id', $user->id],
			['status', 'not_paid_out']
		])->exists()
		) {
			Loan::where([
				['loan_id', $loan_id],
				['user_id', $user->id],
				['status', 'not_paid_out']
			])->delete();

			Deposit::where([
				['loan_id', $loan_id],
				['user_id', $user->id]
			])->update(['status' => 'lost']);

			Installments::where([
				['loan_id', $loan_id],
				['user_id', $user->id],
				['status', 'paid']
			])->update(['status' => 'lost']);

			$deposit = Deposit::where([
				['loan_id', $loan_id],
				['user_id', $user->id]
			])->first();

			$message = [
				['status' => 'error', 'message' => 'Twoja pożyczka została usunięta. Straciłeś swój depozyt o wartości ' . $deposit->amount . ' zł.']
			];

			session()->flash('flash', $message);

			return back();
		} elseif (Loan::where([
			['loan_id', $loan_id],
			['user_id', $user->id],
			['status', 'paid_out']
		])->exists()
		) {
			$loan = Loan::where([
				['loan_id', $loan_id]
			])->first();

			Loan::where([
				['loan_id', $loan_id],
				['user_id', $user->id],
				['status', 'paid_out']
			])->delete();

			Installments::where([
				['loan_id', $loan_id],
				['user_id', $user->id],
				['status', 'paid']
			])->update(['status' => 'lost']);

			Deposit::where([
				['loan_id', $loan_id],
				['user_id', $user->id]
			])->update(['status' => 'lost']);

			$deposit = Deposit::where([
				['loan_id', $loan_id],
				['user_id', $user->id]
			])->first();

			$message = [
				['status' => 'danger', 'message' => 'Twoja pożyczka została usunięta. Straciłeś swój depozyt o wartości ' . $deposit->amount . ' zł. Oddałeś pożyczkę o wartośći '.($loan->amount - $loan->interests).' zł.']
			];

			session()->flash('flash', $message);

			return back();
		} else {
			$message = [
				['status' => 'danger', 'message' => 'Wystąpił błąd.']
			];

			session()->flash('flash', $message);

			return back();
		}
	}
}