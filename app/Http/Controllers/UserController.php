<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
	public function getUser($id)
	{
		$user = User::where([
			['id', $id]
		])->first();

		if (!get_class($user) == 'App\User') {
			return view('errors.404');
		} else {
			return view('panel.user', ['user' => $user]);
		}
	}

	public function submitUser($id, Request $request)
	{
		$user = Auth::user();
		if (!empty($request->input('email'))) $data['email'] = $request->input('email');
		if (!empty($request->input('name'))) $data['name'] = $request->input('name');
		if (!empty($request->input('surname'))) $data['surname'] = $request->input('surname');
		}
}