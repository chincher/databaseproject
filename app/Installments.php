<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Installments extends Model
{
	protected $primaryKey = 'installments_id';
	protected $table = 'installments';

	public function user()
	{
		return $this->belongsTo('App\User', 'id', 'user_id');
	}

	public function loan()
	{
		return $this->belongsTo('App\Installments', 'loan_id', 'loan_id');
	}
}
