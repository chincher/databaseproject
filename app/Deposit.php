<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
	protected $primaryKey = 'deposit_id';
	protected $table = 'deposit';

	public function user()
	{
		return $this->belongsTo('App\User', 'id', 'user_id');
	}


}
