@extends('layouts.app')

@section('content')
    <div class="editForm">
        {!! Form::open(['url' => 'user/'. $user->id .'/edit']) !!}
        {!! Form::label('email', 'E-Mail Address') !!}
        {!! Form::text('email', $user->email,  array('placeholder'=>'Email')) !!}
        <br>
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', $user->name,  array('placeholder'=>'Name')) !!}
        <br>
        {!! Form::label('surname', 'Surname') !!}
        {!! Form::text('Surname', $user->surname    ,  array('placeholder'=>'Surname')) !!}
        <br>
        {!! Form::submit('Edytuj') !!}
        {!! Form::close() !!}
    </div>

@endsection