@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-9 text-left">
            <h1 class="loan-header">Lista pożyczek</h1>
            <h2 class="loan-header">Suma dostępnych środków: {{$sum}} zł</h2>
        </div>
        <div class="col-md-3 text-right">
            <a href="{{url('home')}}">
                <button type="button" class="btn btn-info back">Powrót</button>
            </a>
        </div>
        @if(count($loans) == 0)
            <h2>Nie ma jeszcze żadnych pożyczek</h2>
        @else
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Imię</th>
                    <th>Nazwisko</th>
                    <th>Kwota pożyczki</th>
                    <th>Ilość rat (zapłacono / wszystkie)</th>
                    <th>Status</th>
                    <th>Akcja</th>
                </tr>
                </thead>
                <tbody>
                @foreach($loans as $loan)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$loan->name}}</td>
                        <td>{{$loan->surname}}</td>
                        <td>{{$loan->amount}} zł</td>
                        <td>{{$loan->paid_installments_count}} / {{$loan->installments_number}}</td>
                        <td> @if($loan->status == 'paid_out')
                                <span class="label label-success">Wypłacona</span>
                            @else
                                <span class="label label-default">Niewypłacona</span>
                            @endif
                        </td>
                        <td>
                            @if($loan->status == 'paid_out')
                                <button type="button" class="btn btn-success btn-sm disabled">Wypłacona</button>
                            @else
                                <a href="{{url('admin/loan')}}/{{$loan->loan_id}}/paidout">
                                    <button type="button" class="btn btn-primary btn-sm">Wypłać pożyczkę</button>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>

@endsection