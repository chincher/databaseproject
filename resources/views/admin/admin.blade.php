@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>Suma dostępnych środków: {{$sum}} zł</h1>
        <h2>Suma środków w depozycie: {{$sumOfDeposit}} zł</h2>

        <div class="row">
            <a href="{{url('admin/loans')}}">
                <button type="button" class="btn btn-primary btn-sm">Wypłać pożyczkę</button>
            </a>
        </div>
    </div>



    @endsection