@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-5 text-left">
            <h1>Lista pożyczek</h1>
        </div>
        <div class="col-md-7 text-right">
            <a href="{{url('home')}}">
                <button type="button" class="btn btn-info back">Powrót</button>
            </a>
        </div>
        @if(count($loans) == 0)
            <h2>Nie masz jeszcze żadnych pożyczek</h2>
        @else
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Kwota pożyczki</th>
                    <th>Ilość rat (zapłacono / wszystkie)</th>
                    <th>Status</th>
                    <th>Akcja</th>
                </tr>
                </thead>
                <tbody>
                @foreach($loans as $loan)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$loan->amount}} zł</td>
                        <td>{{$loan->paid_installments_count}} / {{$loan->installments_number}}</td>
                        <td>
                            @if($loan->status == 'paid_out')
                                <span class="label label-success">Wypłacona</span>
                            @else
                                <span class="label label-default">Niewypłacona</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{url('user/loan')}}/{{$loan->loan_id}}/installments">
                                <button type="button" class="btn btn-primary btn-sm">Spłać raty</button>
                            </a>
                            <a href="{{url('user/loan')}}/{{$loan->loan_id}}/delete">
                                <button type="button" class="btn btn-danger btn-sm">Zrezygnuj</button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>

@endsection