@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-5 text-left">
                <h1>Raty pożyczki</h1>
            </div>
            <div class="col-md-7 text-right">
                <a href="{{url('user/loans')}}">
                    <button type="button" class="btn btn-info back">Powrót</button>
                </a>
            </div>

        </div>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Kwota raty</th>
                <th>Data spłaty raty</th>
                <th>Status</th>
                <th>Akcja</th>
            </tr>
            </thead>
            <tbody>
            @foreach($installments as $installment)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$installment->amount}} zł</td>
                    <td>{{$installment->pay_time}}</td>
                    <td>
                        @if($installment->status == 'paid')
                            <span class="label label-success">Zapłacona</span>
                        @elseif($installment->status == 'not_paid')
                            <span class="label label-default">Niewypłacona</span>
                        @elseif($installment->status == 'overdue')
                            <span class="label label-danger">Niewypłacona</span>
                        @endif
                    </td>
                    @if($installment->status == 'paid')
                        <td>
                            <button type="button" class="btn btn-success btn-sm disabled">Zapłacona</button>
                        </td>
                    @else
                        <td>
                            <a href="{{url('user/installment')}}/{{$installment->installments_id}}/pay">
                                <button type="button" class="btn btn-primary btn-sm">Zapłać</button>
                            </a>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection