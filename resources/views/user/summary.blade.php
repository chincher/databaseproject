@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Podsumowanie</h1>
        <p>Kwota pożyczki: {{$loan->amount}} zł</p>
        <p>Status pożyczki: @if($loan->status == 'paid_out')
                <span class="label label-success">Wypłacona</span>
            @else
                <span class="label label-default">Niewypłacona</span>
            @endif
        </p>
        <p>Kwota pożyczki z odsetkami: {{$loan->amount + $loan->interests}} zł</p>
        <p>Data końca pożyczki: {{$loan->loan_end_date}}</p>
        <p>Kwota wpłacona do depozytu: {{$deposit->amount}}</p>
        <p>Kwota pojedyńczej raty: {{$installment->amount}}</p>
        <p>Data wpłaty pierwszej raty: {{$installment->pay_time}}</p>
        <div class="col-md-7 text-right">
            <a href="{{url('user/loans')}}">
                <button type="button" class="btn btn-info back">Powrót</button>
            </a>
        </div>
    </div>

@endsection