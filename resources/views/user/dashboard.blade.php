@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="flex-center">
            <a href="{{url('loan/get')}}">
                <button type="button" class="btn btn-primary btn-lg">Weź pożyczkę</button>
            </a>
            <a href="{{url('user/loans')}}">
                <button type="button" class="btn btn-primary btn-lg">Twoje pożyczki</button>
            </a></div>

    </div>

@endsection