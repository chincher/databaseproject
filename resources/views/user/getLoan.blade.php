@extends('layouts.app')

@section('content')
    <div class="editForm">
        {!! Form::open(['url' => 'loan/get', 'method' => 'post']) !!}
        {!! Form::label('value', 'Kwota pożyczki') !!}
        {!! Form::text('value', '',  array('placeholder'=>'Kwota pożyczki')) !!}
        <br>
        {!! Form::label('installments_count', 'Liczba rat') !!}
        {!! Form::number('installments_count', '',  array('placeholder'=>'Liczba rat')) !!}
        <br>
        {!! Form::submit('Weź pożyczkę!', ['class' => 'btn btn-default']) !!}
        {!! Form::close() !!}
    </div>

@endsection