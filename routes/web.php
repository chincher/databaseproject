<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('customers', 'UserController@getUsers')->middleware('admin');
Route::get('user/{id}/edit', 'UserController@getUser')->middleware('auth');
Route::post('user/{id}/edit', 'UserController@submitUser')->middleware('auth');
Route::get('loan/get', 'LoanController@getLoan')->middleware('auth');
Route::post('loan/get', 'LoanController@submitLoan')->middleware('auth');
Route::get('admin', 'AdminController@adminPanel')->middleware('admin');
Route::get('admin', 'AdminController@adminPanel')->middleware('admin');
Route::get('admin/loans', 'AdminController@getLoansList')->middleware('admin');
Route::get('admin/loan/{loan_id}/paidout', 'AdminController@loanPaidOut')->middleware('admin');
Route::get('user/loans', 'LoanController@getUserLoansList')->middleware('auth');
Route::get('user/loan/{loan_id}/installments', 'InstallmentsController@getUserInstallmentsList')->middleware('auth');
Route::get('user/loan/{loan_id}/delete', 'LoanController@deleteLoan')->middleware('auth');
Route::get('user/installment/{installments_id}/pay', 'InstallmentsController@payInstallment')->middleware('auth');




Auth::routes();

Route::get('/home', 'HomeController@index');
